Installation:
- 
Step 1:
- Run install_python.bat
- There will be a permission request popup

Step 2:
- Run install_pip.bat

Step 3:
- Run install_dependencies.bat

Step 4:
- Go into "Resource" folder and edit "config.properties"
- Add bot token and chat id without space

Interpreting:
-
Data scraped will be in the Resources/scraped_data.txt. This will also be
used to check if the particular article has already been alerted to 
the user.

Data must be on individual lines and will automatically be inserted
by the program on a successful alert.

Logs are in scraper.log

Delay (minutes) between scraping can also be set in the config file.

Keyword to scrap can also be set in the config file.