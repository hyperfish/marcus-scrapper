set PYTHON_VERSION=3.12.0

set INSTALL_DIRECTORY=%USERPROFILE%\AppData\Local\Programs\Python\Python%PYTHON_VERSION%

curl -o python-installer.exe https://www.python.org/ftp/python/%PYTHON_VERSION%/python-%PYTHON_VERSION%-amd64.exe

start /wait python-installer.exe /quiet InstallAllUsers=1 PrependPath=1

python --version

