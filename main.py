import configparser
import time
import bs4 as bs
import requests
from datetime import datetime
import logging

url = "https://clubsnap.com/"
search_str = "Ricoh"
data_file_path = "Resources/scraped_data.txt"
config_file_path = "Resources/config.properties"
bot_token = ""
chat_id = ""
delay_duration = 5  # minutes

logging.basicConfig(
    filename='Resources/scraper.log',
    level=logging.INFO,
    format='%(asctime)s - %(levelname)s - %(message)s'
)


def scrape_page():
    # Read file to check if header exist
    with open(data_file_path, 'r') as file:
        HEADERS = [line.strip() for line in file.readlines()]

    # Scrape the website
    res = requests.get(url)
    if res.status_code == 200:
        try:
            soup = bs.BeautifulSoup(res.text, features="html.parser")
            # Search for article container
            class_name = 'porta-article-item'
            all_divs_with_class = soup.find_all('div', class_=class_name)
            for div in all_divs_with_class:
                # Read header
                desired_header = div.find('h2')
                if desired_header:
                    header_text = desired_header.get_text(strip=True)
                    # Check if keyword exist and is not already in the list
                    if search_str in header_text and header_text not in HEADERS:
                        logging.info(f"Found some data: Header Text: {header_text}")
                        with open(data_file_path, 'a') as file:
                            file.write(header_text + "\n")
                        send_bot_message(
                            "Found: {} -- on {}".format(header_text, datetime.now().strftime("%d-%m-%Y %H:%M:%S")))
        except Exception:
            logging.exception("An exception has occured:")
    else:
        logging.error("HTML error: {} {}".format(res.status_code, res.text))


def send_bot_message(message):
    logging.info("Sending tele message: " + message)
    api_url = f'https://api.telegram.org/bot{bot_token}/sendMessage'
    params = {
        'chat_id': chat_id,
        'text': message,
    }
    response = requests.post(api_url, params=params)


def initialize_vars():
    logging.info("--- Initializing ---")
    global bot_token
    global chat_id
    global search_str
    global config_file_path
    global delay_duration
    config = configparser.ConfigParser()
    config.read(config_file_path)
    bot_token = config.get('Configuration', 'bot_token')
    chat_id = config.get('Configuration', 'chat_id')
    search_str = config.get('Configuration', 'search_str')
    delay_duration = config.getint('Configuration', 'delay_duration')
    logging.info("Initialized with the following: ")
    logging.info("bot_token: " + bot_token)
    logging.info("chat_id: " + chat_id)
    logging.info("search_str: " + search_str)
    logging.info("delay_duration: {}".format(delay_duration))


def main():
    initialize_vars()
    global delay_duration
    while True:
        scrape_page()
        logging.info("Scrapping done")
        time.sleep(60 * delay_duration)


if __name__ == '__main__':
    main()
